+++
title = "Redox RFCs"
+++

Many changes, including bug fixes and documentation improvements can be
implemented and reviewed via the normal GitHub pull request workflow.

Some changes though are "substantial", and we ask that these be put
through a bit of a design process and produce a consensus among the Redox
community and the sub-teams.

The "RFC" (request for comments) process is intended to provide a
consistent and controlled path for new features to enter the language
and standard libraries, so that all stakeholders can be confident about
the direction the language is evolving in.


[Read full documentation on Gitlab](https://gitlab.redox-os.org/redox-os/rfcs)

